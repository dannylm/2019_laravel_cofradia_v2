@extends('layouts.layout')
	@section('content')
	@if(session()->get('success'))
		<div>
		{{ session()->get('success') }}
		</div>
	@endif
	<div>
		<a class="btn btn-success" href="{{ route('tesoreria.create') }}">Añadir</a>
		<table class="table table-hover">
			<thead class="thead-dark">
				<th scope="col">Nombre</th>
				<th scope="col">Descripcion</th>
				<th scope="col">Precio</th>
				<th scope="col">Fecha</th>
				<th scope="col">Ver</th>
				<th scope="col">Editar</th>
				<th scope="col">Eliminar<th>
			</thead>
			<tbody>
				<?php foreach($listadoTesoreria as $x){?>
				
				<tr>
					<td><?php echo $x->nombre ?></td>
					<td><?php echo $x->description ?></td>
					<td><?php echo $x->precio ?></td>
					<td><?php echo $x->fecha ?></td>
					<td><a class="btn btn-primary" href="tesoreria/show/<?php echo $x->id ?>">Ver</a></td>
					<td><a class="btn btn-info" href="tesoreria/edit/<?php echo $x->id ?>">Editar</a></td>
					<td><form id="formulario<?php echo $x->id ?>" method="post" action="{{ route('tesoreria.destroy' , $x->id) }}">
						@METHOD('DELETE')
						@csrf
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $x->id ?>">Eliminar</button>
						<!-- Modal -->
						<div class="modal fade" id="exampleModal<?php echo $x->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Confirmación</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        Estas seguro que quieres eliminar este registro
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Rechazar</button>
						        <button name="comprobar" id="formulario<?php echo $x->id ?>" type="submit" class="btn btn-primary">Eliminar</button>
						      </div>
						    </div>
						  </div>
						</div>
						</form>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(e) {

		  var elBotones = document.getElementsByName("comprobar");

		  /*Asignamos  función para escuchar*/
		  for (var i = 0; i < elBotones.length; i++) {
		    elBotones[i].addEventListener("click", manejarBotones, false)
		  }

		});

		/*function idformulario(){
			var id=this.id;
		}*/
		/*Podremos usar  this.id  para identificar cada botón*/
		function manejarBotones(e) {
		  e.preventDefault();
		  var idForm=this.id;
			document.getElementById(idForm).submit();
		}
	</script>
	@endsection