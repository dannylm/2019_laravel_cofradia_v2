<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<style>
		.card{

		}
	</style>
</head>
<body>
<div classs="container">
<nav>
	<div class="nav-wrapper">
		<a href="#" class="brand-logo">Cofradia</a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="{{ route('socios.index') }}">Socio</a></li>
			<li><a href="{{ route('tesoreria.index') }}">Tesoreria</a></li>
			<li><a href="{{ route('inventario.index') }}">Inventario</a></li>
			<li><a href="{{ route('cuotas.index') }}">Cuota</a></li>
		</ul>
	</div>
</nav>
<br>
<br>
<div class="row">
	<div class="col m3 offset-m2">
		<div class="section">
			<h5>Nombre</h5>
			<p>{{ $tesoreria->nombre }}</p>
		</div>
		<div class="divider"></div>
		<div class="section">
			<h5>Telefono</h5>
			<p>{{ $tesoreria->telefono }}</p>
		</div>
		<div class="divider"></div>
		<div class="section">
			<h5>Fecha</h5>
			<p>{{ $tesoreria->fecha }}</p>
		</div>
		<div class="divider"></div>
		<div class="section">
			<h5>Precio</h5>
			<p>{{ $tesoreria->precio }}</p>
		</div>
		<div class="divider"></div>
		<div class="section">
			<h5>Factura</h5>
			<div class="card">
				<div class="card-image">
					<p><?php if (!empty($tesoreria->fotoFactura)){ ?>
						<img id="factura" src=" {{ asset('storage/fotos_factura/'.$tesoreria->fotoFactura) }}"/>
                        <?php } ?></p>
				</div>
				<div class="card-content">
					<p>{{ $tesoreria->description }}</p>
				</div>
			</div>
		</div>
	</div>
</div>
<a class="waves-effect waves-light btn" href="{{ route('tesoreria.index') }}"> Volver</a>
</div>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>

