<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearSociosTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		    Schema::create('socios', function (Blueprint $table) {
				$table->increments('id');
				$table->string('dni');
				$table->string('nombre');
				$table->string('apellidos');
				$table->string('telefono');
				$table->date('fecha_de_nacimiento');
				$table->string('localidad')->default('Almansa');
				$table->string('provincia')->default('Albacete');
				$table->string('codigo_postal')->default('02640');
				$table->longText('direccion');
				$table->string('pais')->default('España');
				$table->boolean('baja');
				$table->longText('observaciones');
				$table->timestamps();
				$table->engine = 'InnoDB';
				$table->charset = 'utf8';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('socios');
    }
}
