<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuota;
class CuotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $listadoCuotas= Cuota::all();
        return view('cuotas.index',['listadoCuotas'=>$listadoCuotas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cuotas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request ->validate([
            'nombre' => 'required',
            'valor' => 'required',
            'concepto' => 'required',
            'fecha_cobro' => 'required',
            'imagen' => 'mimes:jpg,jpeg,png,gif'

        ]);


        $fichero = $request->file('imagen');
        $nombre_fichero=$fichero->getClientOriginalName();

        $path = 'fotos_cuotas';
        $nombre_fichero = uniqid('cuota',true).'_'.$nombre_fichero;
        $request->file('imagen')->move($path,$nombre_fichero);

        $cuota = new cuota([
            'nombre' => $request->get('nombre'),
            'valor' => $request->get('valor'),
            'concepto' => $request->get('concepto'),
            'fecha_cobro' => $request->get('fecha_cobro'),
            'rutaFichero' => $nombre_fichero
        ]);
        $cuota->save();
        return redirect('/cuotas')->with('success','Los datos se han guardado con éxito');
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuota = Cuota::find($id);
        return view('cuotas.show',compact('cuota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuota = Cuota::find($id);

        return view('cuotas.edit',compact('cuota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request ->validate([
            'nombre' => 'required',
            'valor' => 'required',
            'concepto' => 'required',
            'fecha_cobro' => 'required',
            'imagen' => 'mimes:jpg,png,gif,jpeg'
        ]);

        $fichero = $request->file('imagen');
        $nombre_fichero=$fichero->getClientOriginalName();

        $path = 'fotos_cuotas';
        $nombre_fichero = uniqid('cuota',true).'_'.$nombre_fichero;
        $request->file('imagen')->move($path,$nombre_fichero);


        $cuota = Cuota::find($id);
        $cuota -> nombre = $request->get('nombre');
        $cuota -> valor = $request->get('valor');
        $cuota -> concepto = $request->get('concepto');
        $cuota -> fecha_cobro = $request->get('fecha_cobro');
        $cuota -> rutaFichero = $nombre_fichero;
        $cuota ->save();
        return redirect('/cuotas')->with('success','Los campos han sido actualizados con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cuota = Cuota::find($id);
        $cuota->delete();

        return redirect('/cuotas')->with('success', 'El registro ha sido eliminado');
    }
}
